from setuptools import setup, find_packages


__VERSION__ = '0.1'


setup(
	name='exchange-investments',
	version=__VERSION__,
	description='Exchange investments package',
	author='Juliano Gouveia',
	author_email='juliano@neosacode.com',
	keywords='exchange, investments, neosacode, coins',
	packages=find_packages(exclude=[]),
	python_requires='>=3.5'
)